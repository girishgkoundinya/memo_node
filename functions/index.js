const admin = require('firebase-admin');
const functions = require('firebase-functions');
admin.initializeApp(functions.config().firebase);

exports.CreateMemos = functions.https.onRequest((request, response) => {
    var result = {}
    var db = initFirebase();
    var setMemo = {
        title: request.body.title,
        note: request.body.note,
        created_at: admin.firestore.FieldValue.serverTimestamp()
    };
    if (request.body.participants) {
        setMemo.participants = request.body.participants.split(',');
    }
    var queryPost = db.collection('memos').add(setMemo).then((snapshot) => {
        snapshot.forEach(doc => {
            return null
        });
        return null
    })
    .catch(err => {
        console.log(err)
    });
    queryPost.then((results) => {
        response.status(200).send(result)
        return null
    }).catch((err) => {
        console.log(err)
    })});


exports.GetAllMemos = functions.https.onRequest((request, response) => {
    var result = {}
    result.data = []
    var db = initFirebase();
    var memosRef = db.collection('memos');
    var queryPromise = memosRef.get()
        .then(snapshot => {
            snapshot.forEach(doc => {
                var memo = doc.data()
                if (memo.participants !== undefined){
                    memo.participants = memo.participants.toString()
                }
                result.data.push(memo)
            });
            return null
    })
    .catch(err => {
        console.log(err)
    });

    queryPromise.then((results) => {
        response.status(200).send(result)
        return null
    }).catch((err) => {
        console.log(err)
    })
});

exports.FilterMemos = functions.https.onRequest((request, response) => {
    var result = {}
    result.data = []
    var db = initFirebase();
    var memosRef = db.collection('memos');
    var queryPromise = memosRef.get()
    .then(snapshot => {
        result.data = []
        snapshot.forEach(doc => {
            var isValid = false
            var memo = doc.data()            
            if (request.query.note_query !== "" && memo.note.toLowerCase().includes(request.query.note_query.toLowerCase())){
                console.log("note condition matched")
                isValid = true
            }            
            if (memo.participants){
                memo.participants = memo.participants.toString()
                p = memo.participants.toString();
                if (request.query.participants_query !== "" && p.toLowerCase().includes(request.query.participants_query.toLowerCase())){
                    isValid = true
                }
            }
            // var fromDate = new Date(request.query.fromDate)
            // var toDate = new Date(request.query.toDate)
            // var memoDate = new Date(memo.created_at)

            // console.log(fromDate)
            // console.log(toDate)
            // console.log(memoDate)

            // if (memoDate === fromDate || memoDate === toDate){
            //     isValid = true
            // }else if( memoDate > fromDate && memoDate < toDate  ){ 
            //     isValid = true
            // } 
            if (isValid === true){
                result.data.push(memo)
            }
        });
        return null
    }).catch(err => {
        console.log(err)
    });

    queryPromise.then((results) => {
        response.status(200).send(result)
        return null
    }).catch((err) => {
        console.log(err)
    })
});

function initFirebase() {
    var db = admin.firestore();
    return db
}

